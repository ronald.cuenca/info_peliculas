/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Ratings;

/**
 *
 * @author LENOVO
 */
public class controladorRatings {
    Ratings[] rating;
    Integer size;

    public Ratings[] getRating() {
        return rating;
    }

    public void setRating(Ratings[] rating) {
        this.rating = rating;
    }

    public Integer getSize() {
        return size;
    }

    public controladorRatings(Integer Tamano) {
        rating = new Ratings[Tamano];
        size=0;
    }
    
    public void ingresarRating(String Source, String Value){
        Ratings ratin = new Ratings();
        ratin.setSource(Source);
        ratin.setValue(Value);
        rating[size]=ratin;
        size++;  
    }
}
